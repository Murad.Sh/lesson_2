package com.shukur.lesson2;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Task6 {
    public int findingTheSmallestPositiveInteger(int[] arrayOfNumbers) {
        Set<Integer> positiveNumbersSet = Arrays.stream(arrayOfNumbers)
                .filter(n -> n > 0)
                .boxed()
                .collect(Collectors.toSet());

        int[] positiveNumbers = positiveNumbersSet.stream()
                .mapToInt(Integer::intValue)
                .toArray();

        Arrays.sort(positiveNumbers);
        int missingNumber = 0;

        for (int i = 0; i < positiveNumbers.length; i++) {
            if (i + 1 != positiveNumbers[i]) {
                missingNumber = i + 1;
                break;
            }
        }

        if (missingNumber == 0) {
            missingNumber = positiveNumbers[positiveNumbers.length - 1] + 1;
        }
        return missingNumber;
    }
}
