package com.shukur.lesson2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class Lesson2Application {

    public static void main(String[] args) {
        SpringApplication.run(Lesson2Application.class, args);
        Task1 task = new Task1();
        int number = task.getIndexOfLastWord("Isgender eve getmek isteyir");
        System.out.println("Number of elements in last word is " + number);

        Task2 task2 = new Task2();
        int[] reverseArray = task2.reverse(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        System.out.println("Reverse array is " + Arrays.toString(reverseArray));

        Task3 task3 = new Task3();
        boolean isThereDublicateInArray = task3.isThereDublicate(new int[]{1, 2, 3, 8, 5, 6, 7, 8, 9, 10});
        System.out.println("isThereDublicateInArray " + isThereDublicateInArray);

        Task4 task4 = new Task4();
        int missingNumberOfTask4 = task4.findingMissingNumberInArray(new int[]{0, 1, 2, 4, 5, 6, 7, 8});
        System.out.println("Missing number in array is " + missingNumberOfTask4);

        Task5 task5 = new Task5();
        boolean isSame = task5.isTheSame("abba", "cat dog dog cat");
        System.out.println("The answer is " + isSame);

        Task6 task6 = new Task6();
        int missingNumberOfTask6 = task6.findingTheSmallestPositiveInteger(new int[] {3, 10, 0, 20, 5, 2, 1, 4, 8});
        System.out.println("The first positive missing number in array is " + missingNumberOfTask6);    }

}
