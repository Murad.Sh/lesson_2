package com.shukur.lesson2;

import java.util.HashMap;

public class Task5 {
    public boolean isTheSame(String patterns, String words) {
        char[] pattern = patterns.toCharArray();
        String[] word = words.split(" ");

        if (pattern.length != word.length) {
            return false;
        }

        HashMap<Character, String> map = new HashMap<>();

        for (int i = 0; i < pattern.length; i++) {
            char elementOfPattern = pattern[i];
            String elementOfString = word[i];

            if (!map.containsKey(elementOfPattern)) {
                if (map.containsValue(elementOfString)) {
                    return false;
                }
                map.put(elementOfPattern, elementOfString);
            } else if (!map.get(elementOfPattern).equals(elementOfString)) {
                return false;
            }
        }

        return true;
    }
}
