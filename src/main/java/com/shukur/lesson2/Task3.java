package com.shukur.lesson2;

public class Task3 {
    public boolean isThereDublicate(int[] array) {
        int[] arr = array.clone();
        for (int i = 0; i < arr.length; i++) {
            int thisNumber = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                if (thisNumber == arr[j]) {
                    return true;
                }
            }
        }
        return false;
    }
}
