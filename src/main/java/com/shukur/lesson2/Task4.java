package com.shukur.lesson2;

import java.util.Arrays;

public class Task4 {
    public int findingMissingNumberInArray(int[] array) {
        int[] arr = array.clone();
        //boolean[] isPresent = new boolean[target];
        int nextNum = 0;
        int missingNum = 0;
        for (int j : arr) {
                if (j == nextNum) {
                    nextNum++;
                } else {
                    missingNum = nextNum;
                    break;
                }
            }


        /*for (int i = 0; i < target; i++) {
            if (!isPresent[i]) {  // Находим первое отсутствующее число
                return i;
            }
        }*/
        return missingNum;
    }
}
