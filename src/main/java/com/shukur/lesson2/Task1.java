package com.shukur.lesson2;

import java.util.ArrayList;
import java.util.List;

public class Task1 {
    public int getIndexOfLastWord(String word) {
        String s = word;
        int numberOfIndexes = 0;
        for (int i = s.length() - 1; i > 0; i--) {
            if (String.valueOf(s.charAt(i)).equals(" ")) {
                break;
            } else {
                numberOfIndexes ++;
            }
        }
        return numberOfIndexes;
    }
}
