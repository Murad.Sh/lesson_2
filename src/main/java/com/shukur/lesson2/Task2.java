package com.shukur.lesson2;

public class Task2 {
    public int[] reverse(int[] arr) {
        int[] array = arr.clone();
        int start = 0;
        int end = arr.length - 1;
            while (start < end) {
                int variable = array[start];
                array[start] = array[end];
                array[end] = variable;
                start++;
                end--;
            }
        return array;
    }
}
